<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Domain;
use Auth;
use App\Http\Requests\Domain\Search;
use App\Http\Requests\Domain\DomainId;
use App\Order;
use App\Http\Requests\Domain\Show;
use App\Http\Resources\DomainCollection;
use App\Http\Requests\Domain\UserSearch;
use Illuminate\Support\Facades\Mail;
use App\Mail\UnlockOrderReceived;

class DomainController extends ApiController
{
    public $user;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
        $this->middleware(function ($request, $next) {
            if (Auth::check()){
                $this->user = Auth::user();
            }

            return $next($request);
        });
    }
    
    /**
     * @api {get} /domains Domains list(10 per page)
     * @apiGroup Domains
     * @apiHeader {String} Accept application/json 
     * @apiHeader {String} Authorization Bearer+" "+token
     * 
     * @apiParam {String} [search] Search domains by description or category
     * @apiParam {Boolean} filter Set to 1 for domains with same categories as user, 0 for all domains
     * 
     * @apiSuccessExample {json} Success
     *   HTTP/1.1 200 OK
     *   {
     *       "data": [
     *           {
     *               "id": 3,
     *               "link": "http://www.y***o.com",
     *               "email": null,
     *               "description": "Website for jokes",
     *               "instructions": NULL,
     *               "categories": {
     *                   "4": "Business",
     *                   "5": "Career and Education"
     *               },
     *               "price": "90.00",
     *               "discount": "",
     *               "lang": "Macedonian",
     *               "mon_traffic": 3000,
     *               "DA": null,
     *               "RD": null,
     *               "TF": null,
     *               "CF": null,
     *               "DR": null,
     *               "TAT": null,
     *               "unlocked": false
     *           }
     *       ],
     *       "free_unlocks": 5,
     *       "fee": 50
     *       "links": {
     *           "first": "http://links.devs/domains?page=1",
     *           "last": "http://links.devs/domains?page=1",
     *           "prev": null,
     *           "next": null
     *       },
     *       "meta": {
     *           "current_page": 1,
     *           "from": 1,
     *           "last_page": 1,
     *           "path": "http://links.devs/domains",
     *           "per_page": 10,
     *           "to": 1,
     *           "total": 1
     *       }
     *   }
     * 
     * @apiErrorExample {json} Authentication error
     * {
     *     "message": "Unauthenticated",
     *     "code": 401
     * }
     * 
     * @apiErrorExample {json} Internal Server Error
     *    HTTP/1.1 500 Internal Server Error
     */
    public function index(UserSearch $request)
    {
        $search = $request['search'];
        $user_id = $this->user->id;
        $categories = $this->user->categories->pluck('id');
        $filter = $request['filter'];
        
        $callback = function($query) use($categories) {
            $query->whereIn('category_id', $categories);
        };
        
        $callback1 = function($query) use($search) {
            $query->where('name', 'LIKE', "%".$search."%");
        };
        
        $callback2 = function($query) use($user_id){
            $query->where([
                'purchased' => 1,
                'user_id' => $user_id,
                'expired' => FALSE
                ]);
        };
        
        $domains = Domain::with([
            'orders' => function($query) use($user_id) {
            $query->where(['user_id' => $user_id, 'purchased' => 0]);
        }, 'categories'])
                ->whereDoesntHave('orders', $callback2)
                ->when($filter, function($query) use($callback) {
                    $query->whereHas('categories', $callback);
                    
                })
                ->when($search, function($query) use($search, $callback1, $callback2, $callback, $filter){
                    $query->where('description', 'LIKE', "%".$search."%")
                            ->orWhere(function($query) use($callback1, $callback2, $callback, $filter) {
                                $query->whereHas('categories', $callback1)
                                        ->when($filter, function($query) use($callback) {
                                            $query->whereHas('categories', $callback);
                    
                                        })
                                        ->whereDoesntHave('orders', $callback2);
                            });
                })                
                ->orderBy('mon_traffic', 'DESC');
        
        return new DomainCollection($domains->paginate(10));
    }
    
    /**
     * @api {post} /domain/unlock Unlock
     * @apiGroup Domains
     * @apiHeader {String} Accept application/json   
     * @apiHeader {String} Authorization Bearer+" "+token
     * @apiParam {Integer} domain_id Domain id
     *     
     * @apiSuccessExample {json} Success
     *   {
     *       "message": "Domain unlocked. You have 4 free unlocks left for this month.",
     *       "status": true,
     *       "unlocks": 4
     *   }
     * @apiSuccessExample {json} Success
     *   {
     *       "message": "You have no more free unlocks left for this month. If you want to unlock this domain go to checkout.",
     *       "status": false,
     *       "unlocks": 0
     *   }
     * 
     * @apiErrorExample {json} Authentication error
     * {
     *     "message": "Unauthenticated",
     *     "code": 401
     * }
     * 
     * @apiErrorExample {json} Internal Server Error
     *    HTTP/1.1 500 Internal Server Error
     */
    public function unlock(DomainId $request)
    {
        $order_exists = Order::where([
            'domain_id' => $request['domain_id'],
            'user_id' => $this->user->id
            ])->first();
        $order_id = false;
        
        if($order_exists && $order_exists->purchased !== NULL) {
            return $this->errorResponse(['message' => __('Domain is already unlocked.')], 422);
        } elseif($order_exists && $order_exists->purchased === NULL) {
            $order_id = $order_exists->id;
        }
        
        if($this->user->unlocks > 0) {
            $order = Order::firstOrNew(['id' => $order_id]);
            $order->user_id = $this->user->id;
            $order->domain_id = $request['domain_id'];
            $order->purchased = 0;
            $order->unlocked = 1;
            $saved = $order->save();
        } else {
            return response()->json([
                'message' => __('You have no more free unlocks left for this month. If you want to unlock this domain go to checkout.'),
                'status' => FALSE,
                'unlocks' => $this->user->unlocks
                ]);
        }
                    
        if($saved) {
            $this->user->unlocks -= 1;
            $this->user->save();
            
            Mail::to(explode(',', env('ADMIN_EMAILS')))->send(new UnlockOrderReceived($order));
            
            return response()->json([
                'message' => __('Domain unlocked. You have '.$this->user->unlocks.' free unlocks left for this month.'),
                'status' => TRUE,
                'unlocks' => $this->user->unlocks
                ]);
        } else {
            return $this->errorResponse(__('Internal Server Error'), 500, 'unlock');
        }
    }
    
    /**
     * @api {get} /domain/show Checkout Step 1(Show)
     * @apiGroup Orders
     * @apiHeader {String} Accept application/json   
     * @apiHeader {String} Authorization Bearer+" "+token
     * @apiParam {Integer} domain_id Domain id
     * @apiParam {Boolean} order 1 for order, 0 for unlock 
     *
     *     
     * @apiSuccessExample {json} Success
     *   {
     *       "id": 3,
     *       "link": "http://www.y***o.com",
     *       "email": null,
     *       "description": "",
     *       "instructions": null,
     *       "categories": {
     *           "4": "Business",
     *           "5": "Career and Education"
     *       },
     *       "price": "90.00",
     *       "fee": "50.00",
     *       "total": "140.00",
     *       "unlocked": false,
     *       "user_id": 4,
     *       "user_name": "User2"
     *   }
     * 
     * @apiErrorExample {json} Authentication error
     * {
     *     "message": "Unauthenticated",
     *     "code": 401
     * }
     * 
     * @apiErrorExample {json} Internal Server Error
     *    HTTP/1.1 500 Internal Server Error
     */
    public function show(Show $request)
    {
        $user = $this->user;        
        $domain = Domain::findOrFail($request['domain_id']);
        
        $exists = $domain->orders->where('user_id', $user->id)
                ->first();
        $unlocked = ($exists && $exists->purchased !== NULL) ? TRUE : FALSE;
        
        if($request['order']) {
            $price = ($domain->discount) ? $domain->discount : $domain->price;
            $total = $price + Domain::FEE;
            $fee = Domain::FEE;
        } else {
            $price = Domain::UNLOCK;
            $total = $price;
            $fee = 0;
        }
        
        return response()->json([
            'id' => $domain->id,
            'link' => ($unlocked) ? $domain->link : $domain->getLink(),
            'email' => ($unlocked) ? $domain->email : NULL,
            'description' => ($domain->description) ? $domain->description : '',
            'instructions' => ($unlocked) ? $domain->instructions : NULL,
            'categories' => $domain->categories->pluck('name', 'id'),
            'price' => $price,
            'fee' => $fee,
            'total' => number_format($total, 2, '.', ''),
            'unlocked' => $unlocked,
            'user_id' => $user->id,
            'user_name' => $user->name
        ]);
    }
}
