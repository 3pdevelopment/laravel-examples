<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests\Plant\CreatePlantStep1Form;
use App\Http\Requests\Plant\CreatePlantStep2Form;
use App\Http\Requests\Plant\CreatePlantStep3Form;

use App\Crop;
use App\Plant;
use App\Location;
use App\Status;

class PlantController extends Controller
{
    public $user;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
        $this->middleware(function ($request, $next) {
            if (Auth::check()){
                $this->user = Auth::user();
            }

            return $next($request);
        });
    }
    
    /**
     *Show all crops for plants
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->all();
        if (isset($data['term'])) {
            $term = $data['term'];
        } else {
            $term = "";
        }
        
        $crops = Crop::search($term);
        
        $model = new Crop();
        $model->for_mothers = FALSE;
        
        return view('crop.index', [
            'for_mothers' => FALSE,
            'model' => $model,
            'crops' => $crops->appends(Input::except('page')),
            'term' => $term
        ]);
    }
    
    /**
     * Create plant
     * @return \Illuminate\Http\Response
     */
    public function create($crop_id = FALSE)
    {
        $this->authorize('plant_create', Plant::class);
        
        $crop = Crop::find($crop_id);
        if ($crop == NULL) { abort(404); }
        
        //Redirect back if crop is locked
        if (!$this->user->isAdmin() && $crop->locked) {
            Session::flash('error', __('Crop is locked, you cannot create more plants.'));
            return redirect(url("plant/crop/view", ["id" => $crop_id]));
        }
        
        $statuses = Status::getPlantsStatuses(TRUE, ['Sick', 'Dead', 'Missing', 'Trimming', 'Packaged', 'Quarantine']);
        $locations = Location::getPlantsLocations(TRUE, ['Storage Room', 'Quarantine']);
        
        $mothers = Plant::
                where(['is_mother' => 1])
                ->orderBy('id', 'desc')
                ->orderBy('created_at', 'desc')  
                ->whereHas('status', function ($query) {
                    $query->where('name','!=','Dead');
                })
                ->paginate(10);
        
        return view('plant.create', [
            'crop' => $crop,
            'statuses' => $statuses,
            'locations' => $locations,
            'mothers' => $mothers
        ]);
    }
    
    public function mother_load(Request $request)
    {
        $this->authorize('plant_create', Plant::class);
        
        $params = $request->all();
        $last = $params['last'];
        $crop_id = $params['crop'];
        
        $mothers = Plant::
                where(['is_mother' => 1])
                ->where('id', '<', $last)
                ->orderBy('id', 'desc')
                ->orderBy('created_at', 'desc')
                ->whereHas('status', function ($query) {
                    $query->where('name','!=','Dead');
                })
                ->paginate(10);
        
        $mothers_array = [];
        foreach ($mothers as $key => $mother) {
                $mothers_array[$key]['id'] = $mother->id;
                $mothers_array[$key]['number'] = $mother->number;
            if ($mother->used($crop_id)) {
                $mothers_array[$key]['used'] = "<td class='green'>" . __("Used") . "</td>";
            } else {
                $mothers_array[$key]['used'] = "<td class='red'>" . __("Not Used") . "</td>";
            }
        }
        
        if (count($mothers) != 0) {
            return response()->json(['status' => 1, 'mothers' => $mothers_array]);
        } else {
            return response()->json(['status' => 0]);
        }
    }


    /*
     * Create plant Step 1
     */
    public function step_1(CreatePlantStep1Form $form)
    {
        $this->authorize('plant_create', Plant::class);
    }
    
    /*
     * Create plant Step 2
     */
    public function step_2(CreatePlantStep2Form $form)
    {
        $this->authorize('plant_create', Plant::class);
    }
    
    /*
     * Create plant Step 3
     */
    public function step_3(CreatePlantStep3Form $form)
    {
        $this->authorize('plant_create', Plant::class);
    }
    
    /**
     * AJAX action for generating strains
     */
    public function strain(Request $request)
    {
        $strains = [];
        
        $params = $request->all();
        
        $mother_number = $params['mother_number'];
        $total = $params['total'];
        $crop_id = $params['crop_id'];
        
        $mother = Plant::where(['is_mother' => 1, 'number' => $mother_number])
                ->whereHas('status', function ($query) {
                    $query->where('name', '!=', Status::DEAD);
                })
                ->first();
        if ($mother != NULL) {
            $strains = Plant::generatePlantsStrains($mother, $total);
            
            return response()->json(['status' => 1, 'strains' => $strains]);
        } else {
            Session::flash('error', __('Error caused while creating. Please try again.'));
            $redirect = url("plant/crop/view", ["id" => $crop_id]);
            return response()->json(['status' => 1, 'strains' => $strains, 'redirect' => $redirect]);
        }
    }
    
    /**
     * POST action for saving plants. Last step in form.
     */
    public function store(Request $request)
    {
        $this->authorize('plant_create', Plant::class);
        
        $data = $request->all();
        
        $crop_id = $data['crop_id'];
        $mother_number = $data['mother_number'];
        $status_id = $data['status_val'];
        $location_id = $data['location_val'];
        $strains_string = $data['strains'];
        $strains = explode("|", $strains_string);
        
        if ($data['created'] != NULL) {
            $created_at = strtotime($data['created']);
        } else {
            $created_at = time();
        }
        
        $mother = Plant::where(['number' => $mother_number])->first();
        if ($mother != NULL) {
            foreach ($strains as $strain) {
                $strain_arr = explode(" ", $strain);
                if (count($strain_arr == 2)) {
                    $plant = new Plant();
                    $plant->created_at = $created_at;
                    $plant->updated_at = $created_at;
                    
                    $plant->name = $strain;
                    $plant->user_id = $this->user->id;
                    $plant->strain = $strain_arr[0];
                    $plant->key = $strain_arr[1];
                    $plant->number = $strain;
                    $plant->location_id = $location_id;
                    $plant->status_id = $status_id;
                    $plant->mother_id = $mother->id;
                    $plant->is_mother = 0;
                    $plant->crop_id = $crop_id;
                    $saved = $plant->save();
                    $plant->saveHistory($created_at);
                }
            }
        }
        
        if (isset($saved) && $saved == TRUE) {
            Session::flash('success', __('Plants have been successfully created.'));
        } else {
            Session::flash('error', __('Error caused while creating. Please try again.'));
        }
        
        return redirect(url("plant/crop/view", ["id" => $crop_id]));
    }
    
    public function change_post(Request $request) 
    {
        $params = $request->all();
        
        if (isset($params['status_id']) && trim($params['status_id']) != "") {
            $status_id = trim($params['status_id']);
        }
        if (isset($params['location_id']) && trim($params['location_id']) != "") {
            $location_id = trim($params['location_id']);
        }
        
        if (isset($params['created_at']) && trim($params['created_at']) != "") {
            $created_at = strtotime(trim($params['created_at']));
        } else {
            $created_at = time();
        }
        
        if (isset($status_id) || isset($location_id)) {
            $plants_ids = explode("|", $params['plants']);
            $plants = Plant::where(['is_mother' => 0])->whereIn('id', $plants_ids)->get();
            foreach ($plants as $plant) {
                if (isset($status_id)) {
                    $plant->status_id = $status_id;
                }
                if (isset($location_id)) {
                    $plant->location_id = $location_id;
                }
                $plant->save();
                $plant->saveHistory($created_at);
            }
            Session::flash('success', __('Status/Location has been successfully updated.'));
        } else {
            Session::flash('error', __('Status/Location has not been selected.'));
        }
        
        return redirect($params['redirect_url']);
    }
    
    public function view($id)
    {
        $this->authorize('plant_open', Plant::class);
        
        $plant = Plant::find($id);
        if ($plant == NULL) { abort(404); }
        
        $status_dead = Status::where(['name' => 'Dead'])->first();
        $dead_id = $status_dead->id;
        
        $statuses = Status::getPlantsStatuses(TRUE, ['Trimming', 'Packaged']);
        $locations = Location::getPlantsLocations(TRUE, ['Storage Room']);
        $statuses_perm = Status::permissionsPlantsStatuses($statuses);
        $locations_perm = Location::permissionsPlantsLocations($locations);
        
        $status_no_change = Status::no_change();
        
        return view('plant.view', [
            "user" => $this->user,
            "plant" => $plant,
            "statuses" => $statuses_perm,
            "locations" => $locations_perm,
            "status_no_change" => $status_no_change,
            "dead_id" => $dead_id
        ]);
    }
    
    public function remove(Request $request)
    {
        $data = $request->all();
        $plant = Plant::find($data['id']);
        
        if (!is_null($plant)) {
            $crop = $plant->crop;
            $this->authorize('crop_activate', $crop);
            
            $deleted = $plant->delete();
            if ($deleted) {
                return response()->json(['status' => 1, 'message' => __("The plant has been deleted.")]);
            } else {
                return response()->json(['status' => 0, 'message' => __("Error. Please try again later.")]);
            }
        } else {
            return response()->json(['status' => 0, 'message' => __("Plant does not exists.")]);
        }
    }
    
    public function sticker(Request $request)
    {
        $data = $request->all();
        $plants_ids = explode(",", $data['ids']);
        
        $plants = Plant::whereIn('id', $plants_ids)->get();
        
        $view = \View::make('plant.print.stickers', [
                            'plants' => $plants
                        ])->render();
        
        $contents = mb_convert_encoding($view, 'HTML-ENTITIES', "UTF-8");
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($contents)->setPaper('a4');
        return $pdf->stream('stickers.pdf');
        
    }
    
   
    
}
