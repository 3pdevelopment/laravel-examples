<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\PlantHistory;
use App\PlantOrigin;
use App\Status;
use App\Plant;
use Carbon\Carbon;

class Plant extends Model
{
    protected $table = 'plants';
    
    protected $fillable = [
        'name', 'strain', 'key', 'number', 'user_id', 'location_id', 'status_id',
        'mother_id', 'is_mother', 'crop_id', 'package_id', 'origin_id'
    ];
    
    protected $hidden = [
        
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    
    public function location()
    {
        return $this->belongsTo('App\Location', 'location_id', 'id');
    }
    
    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id', 'id');
    }
    
    /*Get this plant history*/
    public function history()
    {
        return $this->hasMany('App\PlantHistory', 'plant_id', 'id')->orderBy('created_at', 'DESC')->orderBy('id', 'DESC');
    }
    
    /*Get crop where this plant belongs to*/
    public function crop()
    {
        return $this->belongsTo('App\Crop', 'crop_id', 'id');
    }
    
    /*Get package where this plant belongs to*/
    public function package()
    {
        return $this->belongsTo('App\Package', 'package_id', 'id');
    }
    
    /*Get plant origin for this plant*/
    public function origin()
    {
        return $this->belongsTo('App\PlantOrigin', 'origin_id', 'id');
    }
    
    /*Get this plant mother model*/
    public function mother()
    {
        return $this->belongsTo('App\Plant', 'mother_id', 'id');
    }
    
    /*Get all childrens of this plant (mothers and plants)*/
    public function childs()
    {
        return $this->hasMany('App\Plant', 'mother_id', 'id')->orderBy('id', 'DESC');
    }
    
    /*Get childrens which are mothers*/
    public function childs_mothers()
    {
        return $this->hasMany('App\Plant', 'mother_id', 'id')->where(['is_mother' => 1])->orderBy('id', 'DESC');
    }
    
    /*Get childrens which are plants*/
    public function childs_plants()
    {
        return $this->hasMany('App\Plant', 'mother_id', 'id')->where(['is_mother' => 0])->orderBy('id', 'DESC');
    }
    
    /*Is this mother used to add plants in crop. Returns True/False*/
    public function used($crop_id)
    {
        $plants_number = Plant::
                where(['is_mother' => 0, 'mother_id' => $this->id, 'crop_id' => $crop_id])
                ->count();
        
        if ($plants_number == 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }


    public static function mothers_search($term, $locations, $statuses)
    {
        $query = Plant::select('plants.*')
                ->with(['status', 'location', 'mother'])
                ->join('statuses', 'plants.status_id', '=', 'statuses.id')
                ->where(['plants.is_mother' => TRUE]);
        
        if (trim($term) != "" ) {
            $query
                ->where(function ($query1) use ($term) {
                    $query1->where('plants.name', 'LIKE', '%'.$term.'%')
                          ->orWhere('plants.number', 'LIKE', '%'.$term.'%');
                });
        }
        
        if (!empty($locations)) {
            $query->whereIn('plants.location_id', $locations);
        }
        if (!empty($statuses)) {
            $query->whereIn('plants.status_id', $statuses);
        }
        
        $mothers = $query
                    ->orderBy('statuses.orders', 'asc')
                    ->orderBy('plants.id', 'desc')
                    ->orderBy('plants.created_at', 'desc')
                    ->get();
        
        return $mothers;
    }

    public static function generateMothersStrains($status_id, $strain=FALSE, $mothers_number=0, $associative = FALSE)
    {
        $strains = [];
        
        if ($strain && $mothers_number) {
            if ($status_id == 1) {//Clone
                $start = Plant::getMotherStartClone($strain);

                for ($i = 0; $i < $mothers_number; $i++) {
                    $strains[$i] = $strain . "," . $start;
                    $start++;
                }
            } else {//Juvenile/Teen/Adult
                $start = Plant::getMotherStartOther($strain);
                
                for ($i = 0; $i < $mothers_number; $i++) {
                    $strains[$i] = $strain . " " . $start;
                    $start++;
                }
            }
        }
        
        if ($associative) {//If you need associative array
            $strain_associative = [];
            foreach ($strains as $strain) {
                $strain_associative[$strain] = $strain;
            }
            return $strain_associative;
        } else {
            return $strains;
        }
    }
    
    public static function generatePlantsStrains($mother, $number)
    {
        $strains = [];
        $last = 0;
        
        if ($mother->childs_plants != NULL) {
            foreach ($mother->childs_plants as $plant) {
                $plant_num_arr = explode("-", $plant->number);
                if (count($plant_num_arr) == 2) {
                    if ($plant_num_arr[1] > $last) {
                        $last = $plant_num_arr[1];
                    }
                }
            }
        }
        
        //Generate Strains
        for ($i=0; $i<$number; $i++) {
            $last++;
            $strains[$i] = strtoupper($mother->number . "-".$last);
        }
        
        return $strains;
    }


    public function saveHistory($created_at = FALSE)
    {
        if (!Auth::guest()) {
            $user_id = Auth::user()->id;
        } else {
            $user_id = $this->user_id;
        }
        
        $history = new PlantHistory();
        $history->plant_id = $this->id;
        $history->user_id = $user_id;
        $history->location_id = $this->location_id;
        $history->status_id = $this->status_id;
        
        if ($created_at) {
            if (Auth::user()->isAdmin()) {
                $current_time_day_start = time() - strtotime(date("Y-m-d"));
                $created_at_current_time = $created_at + $current_time_day_start;
            } else {
                $created_at_current_time = $created_at;
            }
            
            $history->created_at = $created_at_current_time;
            $history->updated_at = $created_at_current_time;
        }
        
        $history->save();
    }
    
    /*Save origin if Adopted and return origin_id; if Clone find origin and return origin_id*/
    public static function setOrigin($mother_id=FALSE, $name="", $type="", $thc="", $cbd="")
    {
        $origin_id = FALSE;
        if ($mother_id) {
            $mother = Plant::where(["id" => $mother_id])->first();
            if ($mother != NULL) {
                $origin_id = $mother->origin_id;
            }
        } else {
            $origin = new PlantOrigin();
            $origin->name = $name;
            $origin->type = $type;
            $origin->thc = $thc;
            $origin->cbd = $cbd;
            $origin->save();
            
            $origin_id = $origin->id;
        }
        
        return $origin_id;
    }
    
    //Function to get start key for "Clone" status
    public static function getMotherStartClone($strain)
    {
        $str_decimal = count(explode(",", $strain));
        
        $mothers = Plant::
                        where(['is_mother' => 1])
                        ->where('mother_id', '!=', 0)
                        ->where('number', 'LIKE', $strain.",%")
                        ->get();
        if (count($mothers) > 0) {
            $last = 0;
            foreach ($mothers as $mother) {
                $this_mother_arr = explode(",", $mother->number);
                if (count($this_mother_arr) == $str_decimal+1) {
                    if ($this_mother_arr[$str_decimal] > $last) {
                        $last = $this_mother_arr[$str_decimal];
                    }
                }
            }
            $start_key = $last+1;
        } else {
            $start_key = 1;
        }
        
        return $start_key;
    }
    
    //Function to get start key for different than "Clone" status
    public static function getMotherStartOther($strain)
    {
        $mothers = Plant::
                        where(['is_mother' => 1, 'mother_id' => 0, 'strain' => $strain])
                        ->orderBy('key', 'desc')
                        ->get();
        
        if (count($mothers) != 0) {
            $max_mother = $mothers[0];
            
            foreach ($mothers as $mother) {
                if (is_numeric($mother->key) && $mother->key > $max_mother->key) {
                    $max_mother = $mother;
                }
            }
            $max_key = $max_mother->key;
        } else {
            $max_key = 0;
        }
        
        return $max_key+1;
    }
    
    //Plant status before dead
    public function statusBeforeDeath()
    {
        foreach ($this->history as $plant_history) {
            if ($plant_history->status_id != 100) {
                return __($plant_history->status->name);
            }
        }
        
        return "";
    }
    
    //number of live plants in the system 
    public static function global_count_live($type)
    {
        $count = Plant::
                where(['is_mother' => $type])
                ->whereHas('crop', function ($query) {
                    $query->where('activated', '!=', null);
                })
                ->whereHas('status', function ($query) {
                    $query->where('name', '!=', Status::DEAD)
                          ->where('name', '!=', Status::PACKAGED);
                })
                ->count();

        return $count;
    }
    
    
    //number of live plants created (activated) for the day  
    public static function count_live_per_day($type,$date)
    {
        $date = new Carbon($date);

        $count = Plant::
                where(['is_mother' => $type])
                ->whereHas('crop', function ($query)use($date) {
                    $query->whereDate('activated', $date);
                })               
                ->count();

        return $count;
    }    
    
    //number of {status} plants for the day  
    public static function count_status_per_day($type,$date,$status)
    {
        $date = new Carbon($date);

        $status_name = Status::where(['name' => $status])->first();
        $status_id = $status_name->id;

        $count = Plant::
                where(['is_mother' => $type])
                ->whereHas('history', function ($query)use($date, $status_id) {
                    $query->whereDate('created_at', $date)
                          ->where(['status_id' => $status_id]);
                })
                ->when($status == Status::DEAD, function ($q)use($status_id, $date) {
                    return $q->where('status_id', $status_id)
                            ->whereDoesntHave('history', function ($query)use($date) {
                                $query->whereDate('created_at', ">", $date);
                            });
                })
                ->count();

        return $count;
    }
    
    //number of created plants in period 
     public static function count_live_per_period($type,$start_date,$end_date)
    {
        $count = Plant::
                where(['is_mother' => $type])
                ->whereHas('crop', function ($query)use($start_date,$end_date) {
                    $query->whereDate('activated','>=', $start_date)
                            ->whereDate('activated','<=', $end_date);
                })               
                ->count();

        return $count;
    }  
    
    //number of {status} plants for the period  
    public static function count_status_per_period($type,$start_date,$end_date,$status)
    {
        $status_name = Status::where(['name' => $status])->first();
        $status_id = $status_name->id;

        $count = Plant::
                where(['is_mother' => $type])
                ->whereHas('history', function ($query)use($start_date,$end_date, $status_id) {
                    $query->whereDate('created_at','>=', $start_date)
                          ->whereDate('created_at','<=', $end_date)
                          ->where(['status_id' => $status_id]);
                })
                ->when($status == Status::DEAD, function ($q)use($status_id,$end_date) {
                    return $q->where('status_id', $status_id)
                            ->whereDoesntHave('history', function ($query)use($end_date) {
                                $query->whereDate('created_at', ">", $end_date);
                            });
                })
                ->count();

        return $count;
    }
}



         
                
 
